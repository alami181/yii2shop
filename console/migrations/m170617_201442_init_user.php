<?php

use yii\db\Migration;

class m170617_201442_init_user extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->insert('{{%user}}', [
            'username' => 'adm!n',
            'auth_key' => 'CeuKm6qT-cARM7ywnDMbHvd8cMA6vtCT',//1q2w3e
            'password_hash' => '$2y$13$o2uzZzl0Je9OBVXbNW02/u9rypLVQGtHks4I55TOMOP0CUiUQ3UyK',
            'email' => 'shop@mail.uz',
//            'phone' => '9234869238',
            'created_at' => time(),
            'updated_at' => time(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
